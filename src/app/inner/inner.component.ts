import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'app-inner',
  templateUrl: './inner.component.html',
  styleUrls: ['./inner.component.scss']
})
export class InnerComponent implements OnInit, OnDestroy {

  index: number;

  private paramSub: Subscription;

  get url() {
    return window.location.href;
  }

  constructor(
    private route: ActivatedRoute,
    private location: Location,
  ) {
    this.paramSub = this.route.paramMap.subscribe({
      next: map => {
        this.index = +map.get('id');
      },
    })
  }

  ngOnInit() {
  }

  ngOnDestroy() {

  }

  goBack() {
    this.location.back();
  }

}
