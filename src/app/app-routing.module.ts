import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OuterComponent } from './outer/outer.component';
import { InnerComponent } from './inner/inner.component';

const routes: Routes = [
  {path: '', component: OuterComponent},
  {path: 'inner/:id', component: InnerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
